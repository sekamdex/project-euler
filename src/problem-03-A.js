/**
 * Ejercicio 3 - https://projecteuler.net/problem=3
 * @return {number}
 */

var currentPrime = 2;
var factors = [];

function primeFactorization(quotient) {
  if(quotient > 1) {
    if (quotient%currentPrime === 0) {
      factors.push(currentPrime);
      quotient = quotient / currentPrime;
    } else {
      currentPrime += 1;
    }
    return primeFactorization(quotient);
  } else {
    return factors;
  }
}

console.log( primeFactorization(600851475143) );
