/**
 * Ejercicio 1 - https://projecteuler.net/problem=2
 * @return {number}
 */

function fibonacciSequence(sequenceLimit) {

  var sequence = [0,1];

  for (var i = 2; sequence[i-1] < sequenceLimit; i++) {
    /**
     * https://es.wikipedia.org/wiki/Sucesi%C3%B3n_de_Fibonacci
     *
     * Los números de Fibonacci quedan definidos por:
     * f(n) = f(n-1) + f(n-2)
     */
    sequence.push( sequence[i-1] + sequence[i-2] );
  }
  return sequence;
}

function sumEven(arr) {

  var sum = 0;

  for (var i = 0; i < arr.length; i++) {

    if (arr[i]%2 === 0) {
      sum += arr[i];
    }
  }
  return sum;
}

var seq = fibonacciSequence(4000000);

var result = sumEven( seq );

console.log( result );
