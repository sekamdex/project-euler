/**
 * Ejercicio 5 - https://projecteuler.net/problem=5
 * NOT FINISHED
 * @return {number}
 */

let evenlyDivisible = () => {
  let a = 1
  let b = []

  let runRemainder = (a) => {
    let arr = []
    for (let i = 1; i < 20; i++) {
      arr.push(a % i)
    }
    return arr
  }

  do {
    b = runRemainder(a)
    a++
  } while ( Math.max(...b) > 0 )

  console.log(a)
}

evenlyDivisible()
