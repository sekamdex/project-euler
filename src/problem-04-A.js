/**
 * Ejercicio 4 - https://projecteuler.net/problem=4
 * @return {number}
 */

let reverseString = str => { return str.split("").reverse().join("") }

let mult = () => {
  let arr = []
  let arrFinal = []
  for (var i = 100; i < 1000; i++) {
    for (var j = 100; j < 1000; j++) {
      let a = (i*j).toString()
      if (a.length > 5) {
        let b = a.substr(0,3)
        let c = reverseString(a.substr(3,3))
        if (b === c) arr.push(a)
      }
    }
  }

  /**
   * https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Operadores/Spread_operator
   * Obtenemos el número más grande del arreglo a través de Math.max
   * y un operador de propagación.
   * @type {Number}
   */
  return Math.max(...arr)
}

console.log( mult() )
