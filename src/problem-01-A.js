/**
 * Ejercicio 1 - https://projecteuler.net/problem=1
 * @return {number}
 */

function suma(max) {
  var result = 0;
  for (var i = max; i >= 0; i--) {
    if (i%3 === 0 || i%5 === 0) { result += i; }
  }
  return result;
}

console.log( suma(999) );
